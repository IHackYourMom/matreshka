import React, { Component } from 'react';
import '../Styles/App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>Matreshka</p>
        </header>
      </div>
    );
  }
}

export default App;
