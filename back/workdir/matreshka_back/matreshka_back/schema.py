import graphene
import graphql_jwt

import quests.schema
import quests.schema_relay
import users.schema

class Query(
    quests.schema.Query, 
    users.schema.Query,
    quests.schema_relay.RelayQuery,
    graphene.ObjectType,
):
    pass

class Mutation(
    quests.schema.Mutation,
    quests.schema_relay.RelayMutation,
    users.schema.Mutation, 
    graphene.ObjectType,
):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()

schema = graphene.Schema(
                            query=Query,
                            mutation=Mutation,
                        )
