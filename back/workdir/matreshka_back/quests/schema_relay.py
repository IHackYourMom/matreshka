import graphene
import django_filters
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import Quest, Vote

class QuestFilter(django_filters.FilterSet):
    class Meta:
        model = Quest
        fields = ['description']


class QuestNode(DjangoObjectType):
    class Meta:
        model = Quest
        interfaces = (graphene.relay.Node,)


class VoteNode(DjangoObjectType):
    class Meta:
        model = Vote
        interfaces = (graphene.relay.Node,)


class RelayCreateQuest(graphene.relay.ClientIDMutation):
    quest = graphene.Field(QuestNode)

    class Input:
        description = graphene.String()

    def mutate_and_get_payload(root, info, **input):
        user = info.context.user or None

        quest = Quest(
            description=input.get('description'),
            created_by=user,
        )
        quest.save()

        return RelayCreateQuest(quest=quest)


class RelayMutation(graphene.AbstractType):
    relay_create_link = RelayCreateQuest.Field()


class RelayQuery(graphene.ObjectType):
    relay_quest = graphene.relay.Node.Field(QuestNode)
    relay_quests = DjangoFilterConnectionField(QuestNode, filterset_class=QuestFilter)
