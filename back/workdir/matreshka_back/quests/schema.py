import graphene
from graphene_django import DjangoObjectType

from .models import Quest, Vote
from users.schema import UserType


class QuestType(DjangoObjectType):
    class Meta:
        model = Quest


class VoteType(DjangoObjectType):
    class Meta:
        model = Vote


class CreateQuest(graphene.Mutation):
    id = graphene.Int()
    description = graphene.String()
    created_by = graphene.Field(UserType)

    class Arguments:
        description = graphene.String()

    def mutate(self, info, description):
        user = info.context.user or None

        quest = Quest(
            description=description,
            created_by=user,
        )
        quest.save()

        return CreateQuest(
            id=quest.id,
            description=quest.description,
            created_by=quest.created_by,
        )


class CreateVote(graphene.Mutation):
    user = graphene.Field(UserType)
    quest = graphene.Field(QuestType)

    class Arguments:
        quest_id = graphene.Int()

    def mutate(self, info, quest_id):
        user = info.context.user
        if user.is_anonymous:
            raise Exception('You must be logged to vote!')

        quest = Quest.objects.filter(id=quest_id).first()
        if not quest:
            raise Exception('Invalid Quest!')

        Vote.objects.create(
                user=user,
                quest=quest,
        )

        return CreateVote(user=user, quest=quest)

class Query(graphene.ObjectType):
    quests = graphene.List(QuestType)
    votes = graphene.List(VoteType)

    def resolve_quests(self, info, **kwargs):
        return Quest.objects.all()

    def resolve_votes(self, info, **kwargs):
        return Vote.objects.all()

class Mutation(graphene.ObjectType):
    create_quest = CreateQuest.Field()
    create_vote = CreateVote.Field()
