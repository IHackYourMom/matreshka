from django.db import models
from django.conf import settings

class Quest(models.Model):
    description = models.TextField(blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)

class Vote(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    quest = models.ForeignKey('quests.Quest', related_name='votes', on_delete=models.CASCADE)
