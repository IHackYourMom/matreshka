django==2.1.1
django-cors-headers==2.4.0
graphene==2.1.3
graphene-django==2.2.0
django-filter==2.0.0
